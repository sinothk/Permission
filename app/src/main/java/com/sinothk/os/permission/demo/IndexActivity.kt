package com.sinothk.os.permission.demo

import android.os.Bundle
import com.sinothk.os.permission.XPermissions
import com.sinothk.os.permission.base.PermissionsBaseActivity

/**
 * 示例代码
 */
class IndexActivity : PermissionsBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index)

        XPermissions.newInstance(this)
                .setOnRequestPermissionsCallback(this)
//            .requestPermissions(getString(R.string.InvalidPermissionText), Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA)
                .requestAllPermissions()
    }

}