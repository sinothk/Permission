package com.sinothk.os.permission.base

import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.sinothk.os.permission.OnRequestPermissionsCallback
import com.sinothk.os.permission.R
import com.sinothk.os.permission.XPermissions
import java.util.*

open class PermissionsBaseActivity : AppCompatActivity(), OnRequestPermissionsCallback {

    override fun onGranted() {
        Toast.makeText(this, R.string.userGrantedAllPermission, Toast.LENGTH_LONG).show()
    }

    override fun onDenied(deniedPermissions: Array<String>) {
        Toast.makeText(this, R.string.userDeniedSomePermission, Toast.LENGTH_LONG).show()
    }

    override fun onPermanentlyDenied(deniedPermissions: Array<String>) {
        AlertDialog.Builder(this)
                .setTitle(R.string.somePermissionsPromptAgain)
                .setMessage(String.format(Locale.CHINA, getString(R.string.deniedPermissions), deniedPermissions.contentToString()))
                .setPositiveButton(R.string.to_open) { _, _ ->
                    XPermissions.jumpToSettingPermissionPage(this)
                }.setNegativeButton(R.string.cancel) { _, _ ->
                    onDenied(deniedPermissions)
                }.setCancelable(false).show()
    }

    override fun onSettingBackDenied(deniedPermissions: Array<String>) {
        Toast.makeText(this, R.string.backSettingsDeniedPermissions, Toast.LENGTH_LONG).show()
    }

}